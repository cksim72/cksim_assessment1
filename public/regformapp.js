// Registration Form App
(function() {
	var RegFormApp = angular.module("RegFormApp", []);

	var initForm = function(ctrl) {
        ctrl.email="";
        ctrl.password="";
        ctrl.name="";
        ctrl.gender="";
        ctrl.dob=new Date();
        ctrl.address="";
        ctrl.country="";
        ctrl.contact="";
	}

	var createQueryString = function(ctrl) {
		return ({
			email: ctrl.email,
			password: ctrl.password,
			name: ctrl.name,
			gender: ctrl.gender,
			dob: ctrl.dob,
			address: ctrl.address,
            country: ctrl.country,
            contact: ctrl.contact
		});
	}

	var haveLowerCase = function(text) {

		for (var i=0; i<text.length; i++) {
			if ((text.charAt(i) >= 'a') && (text.charAt(i) <= 'z')) return (true);
		}

		return (false);
	}

	var haveUpperCase = function(text) {
		
		for (var i=0; i<text.length; i++) {
			if ((text.charAt(i) >= 'A') && (text.charAt(i) <= 'Z')) return (true);
		}

		return (false);
	}

	// Validate Password
	var validatePassword= function(password) {

		if (password.length < 8) return (0);
		if (haveLowerCase(password)==false) return (0);
		if (haveUpperCase(password)==false) return (0);
		if ((password.indexOf("@")==-1) && (password.indexOf("#")==-1) && (password.indexOf("$")==-1)) return (0);
		for (var i=0; i<password.length; i++) {
			if ((password.charAt(i) >= '0') && (password.charAt(i) <= '9')) return (1);
		}

//		console.log("Password OK");
		return (0);
	}

	// Validate Contact
	var validateContact= function(contact) {
		
		var	str=contact;
		var idx=str.indexOf("(");
		
		if (idx) {
			for (var i=idx; i< str.length; i++)
				if (str[i]=="(") str=str.replace("(","");
		}

		var idx=str.indexOf(")");
		if (idx) {
			for (var i=idx; i< str.length; i++)
				if (str[i]==")") str=str.replace(")","");
		}

		var idx=str.indexOf("+");
//		console.log("idx = %d",idx);
		if (idx) {
			for (var i=idx; i< str.length; i++)
				if (str[i]=="+") str=str.replace("+","");
		}

		var idx=str.indexOf("-");
		if (idx) {
			for (var i=idx; i< str.length; i++)
				if (str[i]=="-") str=str.replace("-","");
		}

		var idx=str.indexOf(" ");
		if (idx) {
			for (var i=idx; i< str.length; i++)
				if (str[i]==" ") str=str.replace(" ","");
		}

		// If remaining part is not a number, then there are invalid characters.
		if (isNaN(str)) return (0);

//		console.log("Int: %d", parseInt(str));
		return (1);
	}

	var validateAge= function(dob) {

		var ageDifMs = Date.now() - dob.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		var age = Math.abs(ageDate.getUTCFullYear() - 1970);
	
//		console.log("age; %d",age);

		if (age<18) return (0);

		return (1);
	}

	var validateEmail= function(email) {

		var idx=email.indexOf(".");		// Check for "." as in @gmail.com

		if (idx==-1) return (0);

		return (1);
	}

	// Controller function
	var RegFormCtrl = function($http) {
		var regFormCtrl = this;

		initForm(regFormCtrl);

		regFormCtrl.showDisplay="register";		// Flag to decide to which <div> to show

		regFormCtrl.country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
		

        regFormCtrl.submitForm = function() {

			regFormCtrl.errmsg = [];
			regFormCtrl.errStatus = 0;

			if (validateEmail(regFormCtrl.email)==0)
				regFormCtrl.errmsg.push("Invalid Email address entered.");

			if (validatePassword(regFormCtrl.password) == 0)
				regFormCtrl.errmsg.push("Invalid password entry");

			if (validateAge(regFormCtrl.dob)==0)
				regFormCtrl.errmsg.push("User must be at least 18 years of age");

			if (validateContact(regFormCtrl.contact)==0)
				regFormCtrl.errmsg.push("Invalid contact number. Other than numerical digits, only '+', '\(\)' and <space> are allowed.");
			
//			console.log("DOB stored: %s", regFormCtrl.dob.toLocaleDateString());
			
//			console.log(regFormCtrl.errmsg);

			if (regFormCtrl.errmsg.length==0) {
				$http.get("/submit-form", {
                    params: {
                        email: regFormCtrl.email,
						password: regFormCtrl.password,
						name: regFormCtrl.name,
						gender: regFormCtrl.gender,
						dateOfBirth: regFormCtrl.dob.toLocaleDateString(),
						address: regFormCtrl.address,
						country: regFormCtrl.country,
						contact: regFormCtrl.contact  
                    }          
                }).then(function() {
                    console.log("Form submitted successfully");
					regFormCtrl.showDisplay="success";
					initForm(regFormCtrl);

                }).catch(function(status) {
					console.log("Form submission failed");
					regFormCtrl.showDisplay="fail";
                });
			}
			else regFormCtrl.errStatus=1;
			
        }
	};

	RegFormApp.controller("RegFormCtrl", [ "$http", RegFormCtrl ]);
})();
